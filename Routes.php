<?php
    return [
        App\Core\Route::get('|^user/register/?$|', 'Main', 'getRegister'),
        App\Core\Route::post('|^user/register/?$|', 'Main', 'postRegister'),
        App\Core\Route::get('|^user/login/?$|', 'Main', 'getLogin'),
        App\Core\Route::post('|^user/login/?$|', 'Main', 'postLogin'),
        App\Core\Route::get('|^user/logout/?$|', 'Main', 'logout'),

        App\Core\Route::get('|^about-us/?$|', 'Main', 'about_us'),
        App\Core\Route::get('|^contact/?$|', 'Main', 'contact'),

        App\Core\Route::get('|^category/([0-9]+)/?$|', 'Category', 'show'),

        App\Core\Route::get('|^part/([0-9]+)/?$|', 'Part', 'show'),
        App\Core\Route::get('|^car/([0-9]+)/?$|', 'Car', 'show'),
        App\Core\Route::get('|^manufacturer/([0-9]+)/?$|', 'Manufacturer', 'show'),
        App\Core\Route::post('|^search/?$|', 'Part', 'postSearch'),
        App\Core\Route::post('|^filter/?$|', 'Part', 'postFilter'),
        App\Core\Route::get('|^handle/([a-z]+)/?$|', 'EventHandler', 'handle'),

        #Api rute:
        App\Core\Route::get('|^api/carts/?$|', 'ApiCart', 'showCart'),
        App\Core\Route::get('|^api/carts/add/([0-9]+)/?$|', 'ApiCart', 'addToCart'),
        App\Core\Route::get('|^api/carts/clear/?$|', 'ApiCart', 'clear'),

        #User role routes:
        App\Core\Route::get('|^user/profile/?$|', 'UserDashboard', 'index'),
        #category table:
        App\Core\Route::get('|^user/categories/?$|', 'UserCategoryManagement', 'categories'),
        App\Core\Route::get('|^user/categories/edit/([0-9]+)/?$|', 'UserCategoryManagement', 'getEdit'),
        App\Core\Route::post('|^user/categories/edit/([0-9]+)/?$|', 'UserCategoryManagement', 'postEdit'),
        App\Core\Route::get('|^user/categories/add/?$|', 'UserCategoryManagement', 'getAdd'),
        App\Core\Route::post('|^user/categories/add/?$|', 'UserCategoryManagement', 'postAdd'),
        App\Core\Route::get('|^user/categories/delete/([0-9]+)/?$|', 'UserCategoryManagement', 'delete'),
        #part table:
        App\Core\Route::get('|^user/parts/?$|', 'UserPartManagement', 'parts'),
        App\Core\Route::get('|^user/parts/edit/([0-9]+)/?$|', 'UserPartManagement', 'getEdit'),
        App\Core\Route::post('|^user/parts/edit/([0-9]+)/?$|', 'UserPartManagement', 'postEdit'),
        App\Core\Route::get('|^user/parts/add/?$|', 'UserPartManagement', 'getAdd'),
        App\Core\Route::post('|^user/parts/add/?$|', 'UserPartManagement', 'postAdd'),
        App\Core\Route::get('|^user/parts/delete/([0-9]+)/?$|', 'UserPartManagement', 'delete'),
        #manufacturer table:
        App\Core\Route::get('|^user/manufacturers/?$|', 'UserManufacturerManagement', 'manufacturers'),
        App\Core\Route::get('|^user/manufacturers/edit/([0-9]+)/?$|', 'UserManufacturerManagement', 'getEdit'),
        App\Core\Route::post('|^user/manufacturers/edit/([0-9]+)/?$|', 'UserManufacturerManagement', 'postEdit'),
        App\Core\Route::get('|^user/manufacturers/add/?$|', 'UserManufacturerManagement', 'getAdd'),
        App\Core\Route::post('|^user/manufacturers/add/?$|', 'UserManufacturerManagement', 'postAdd'),
        App\Core\Route::get('|^user/manufacturers/delete/([0-9]+)/?$|', 'UserManufacturerManagement', 'delete'),
        #car table:     
        App\Core\Route::get('|^user/cars/?$|', 'UserCarManagement', 'cars'),
        App\Core\Route::get('|^user/cars/edit/([0-9]+)/?$|', 'UserCarManagement', 'getEdit'),
        App\Core\Route::post('|^user/cars/edit/([0-9]+)/?$|', 'UserCarManagement', 'postEdit'),
        App\Core\Route::get('|^user/cars/add/?$|', 'UserCarManagement', 'getAdd'),
        App\Core\Route::post('|^user/cars/add/?$|', 'UserCarManagement', 'postAdd'),
        App\Core\Route::get('|^user/cars/delete/([0-9]+)/?$|', 'UserCarManagement', 'delete'),
        #cart table:
        App\Core\Route::get('|^user/cart/([0-9]+)/?$|', 'CartPart', 'show'),
        App\Core\Route::get('|^user/cart/?$|', 'Cart', 'home'),
        App\Core\Route::get('|^user/cart/delete/([0-9]+)/?$|', 'Cart', 'delete'),
        App\Core\Route::get('|^cart/?$|', 'UserCartManagement', 'getCart'),
        App\Core\Route::post('|^cart/?$|', 'UserCartManagement', 'postCart'),
        
        App\Core\Route::any('|^.*$|', 'Main', 'home')
    ];