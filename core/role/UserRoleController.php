<?php
    namespace App\Core\Role;
    use App\Core\Controller;

    class UserRoleController extends Controller {
        public function __pre(){
            parent::__pre();

            /*$userId = $this->getSession()->get('user_id', null);

            if($userId === null){
                $this->redirect('../user/login');
            }*/
            if($this->getSession()->get('user_id') === null){
                $this->redirect(\Configuration::BASE . 'user/login');
            }
        }

    }