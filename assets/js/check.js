$(function() {
    $('input[name=is_warranty_given]').on('click init-is_warranty_given', function() {
        if (!$('#is_warranty_given').is(':checked')) {
            $('#warranty_month').val('');
        }
        $('#warranty').toggle($('#is_warranty_given').prop('checked'));
    }).trigger('init-is_warranty_given');
});