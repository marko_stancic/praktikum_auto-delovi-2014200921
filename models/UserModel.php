<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class UserModel extends Model{

        protected function getFields(): array{
            return [
                'user_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'forename' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'surename' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'username' => new Field( (new StringValidator())->setMinLength(5)->setMaxLength(120) ),
                'password' => new Field( (new StringValidator())->setMinLength(7)->setMaxLength(124) ),
                'email' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),

                

            ];
        }
        
        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
            
        }

        public function getByName(string $name){
           return $this->getByFieldName('name', $name);
        }
    }