<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class CartModel extends Model{

        protected function getFields(): array{
            return [
                'cart_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'forename' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'surename' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'email' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(120) ),
                'address' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(32) ),
                'phone' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(32) ),
                

            ];
        }
        
    }