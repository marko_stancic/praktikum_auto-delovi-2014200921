<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class CarModel extends Model{

        protected function getFields(): array{
            return [
                'car_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'model' => new Field( (new StringValidator())->setMinLength(3)->setMaxLength(255) ),
                'manufacturer_id' => new Field( (new NumberValidator())->setIntegerLength(11) )
                

            ];
        }
        
    }