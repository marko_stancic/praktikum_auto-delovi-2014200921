<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use App\Validators\WarrantyValidator;

    class PartModel extends Model{

        protected function getFields(): array{
            return [
                'part_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'name' => new Field( (new StringValidator())->setMaxLength(255) ),
                'description' => new Field( (new StringValidator())->setMaxLength(6*1024) ),
                'price' => new Field( (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2) ),
                'category_id' => new Field( (new NumberValidator())->setIntegerLength(11) ),
                'is_in_stock' => new Field( new BitValidator() ),
                'is_warranty_given' => new Field( new BitValidator() ),
                'warranty_month' => new Field( (new WarrantyValidator())->setIntegerLength(11) ),
                'car_id' => new Field( (new NumberValidator())->setIntegerLength(11) ),
                'user_id' => new Field( (new NumberValidator())->setIntegerLength(11) ),
                'image_path' => new Field( (new StringValidator())->setMaxLength(120) ),
                

            ];
        }
        
        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
            
        }

        public function getByName(string $name){
           return $this->getByFieldName('name', $name);
        }

        public function getAllBySearch(string $keywords) {
            $sql = 'SELECT * FROM `part` WHERE `name` LIKE ? OR `description` LIKE ?;';
            $keywords = '%' . $keywords . '%';

            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$keywords, $keywords]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

        public function getAllByFilter(int $carId, int $categoryId, int $manufacturerId, int $price, int $price_to) {
            $sql = 'SELECT * FROM`manufacturer` 
                    INNER JOIN `car` ON manufacturer.manufacturer_id = car.manufacturer_id 
                    INNER JOIN `part` ON car.car_id = part.car_id 
                    INNER JOIN `category` ON category.category_id = part.category_id 
                    WHERE car.car_id = ? AND category.category_id = ? AND manufacturer.manufacturer_id = ? AND part.price BETWEEN ? AND ? ORDER BY part.price ASC;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$carId, $categoryId, $manufacturerId,  $price, $price_to]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
        public function getAllByPartId(int $partId) {
            $sql = 'SELECT * FROM `parts_show` WHERE part_id = ?;';     
            $prep = $this->getConnection()->prepare($sql);

            if(!$prep){
                return [];
            }

            $res = $prep->execute([$partId]);
            if(!$res){
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }

    }