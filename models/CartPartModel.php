<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class CartPartModel extends Model{

        protected function getFields(): array{
            return [
                'cart_part_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'cart_id' => new Field( (new NumberValidator())->setIntegerLength(11) ),
                'part_id' => new Field( (new NumberValidator())->setIntegerLength(11) )
                

            ];
        }   

 
    }