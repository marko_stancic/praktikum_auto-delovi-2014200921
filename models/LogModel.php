<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;

    class LogModel extends Model{

        protected function getFields(): array{
            return [
                'log_id' => new Field( (new NumberValidator())->setIntegerLength(20), false ),
                'created_at' => new Field( (new DateTimeValidator())->allowDate()->allowTime() , false ),

                'user_id' => new Field( (new NumberValidator())->setIntegerLength(11) ),
                'ip_address' => new Field( (new StringValidator())->setMaxLength(24) )
                

            ];
        }

        
    }