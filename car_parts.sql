-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2018 at 04:52 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_parts`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `car_id` int(10) UNSIGNED NOT NULL,
  `manufacturer_id` int(10) UNSIGNED DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`car_id`, `manufacturer_id`, `model`, `created_at`) VALUES
(1, 1, 'focus', '2018-06-16 18:22:12'),
(2, 2, 'saxo', '2018-06-16 18:21:45'),
(3, 6, 'a5', '2018-06-16 18:21:57'),
(6, 1, 'c-max', '2018-06-19 18:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `forename` varchar(255) DEFAULT NULL,
  `surename` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `created_at`, `forename`, `surename`, `email`, `address`, `phone`) VALUES
(16, '2018-06-11 10:18:46', 'Marko', 'Markovic', 'stancic6yty@gmail.com', 'ghjghjgh 8', ''),
(18, '2018-06-11 10:18:50', 'Marko', 'Stancic', 'stancic6ffd@gmail.com', 'fdgdfgdfgdf 43536', ''),
(27, '2018-06-14 21:22:03', 'abc', 'abc', 'sda@sa.sad', 'abcdedsad', '1551555515');

-- --------------------------------------------------------

--
-- Table structure for table `cart_part`
--

CREATE TABLE `cart_part` (
  `cart_part_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED DEFAULT NULL,
  `part_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart_part`
--

INSERT INTO `cart_part` (`cart_part_id`, `cart_id`, `part_id`, `created_at`) VALUES
(1, 16, 5, '2018-06-06 11:52:37'),
(2, 16, 6, '2018-06-06 11:52:37'),
(3, 18, 5, '2018-06-06 11:54:46'),
(4, 18, 6, '2018-06-06 11:54:46'),
(5, 18, 36, '2018-06-06 11:54:46'),
(6, 27, 31, '2018-06-14 21:22:03');

-- --------------------------------------------------------

--
-- Stand-in structure for view `cart_part_view`
-- (See below for the actual view)
--
CREATE TABLE `cart_part_view` (
`cart_part_id` int(10) unsigned
,`name` varchar(255)
,`created_at` timestamp
,`forename` varchar(255)
,`surename` varchar(255)
,`address` varchar(255)
,`email` varchar(255)
,`cart_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `created_at`) VALUES
(1, 'Rasveta', '2018-06-19 18:40:33'),
(2, 'Retrovizori', '2018-06-19 18:40:40'),
(3, 'Stakla', '2018-06-19 18:40:47'),
(4, 'Motor', '2018-06-19 18:40:55'),
(12, 'Rashladni sistemi', '2018-06-19 18:41:35'),
(13, 'Karoserija', '2018-06-19 18:41:54'),
(14, 'Vešanje i pogon', '2018-06-19 18:42:34'),
(15, 'Dodatna oprema', '2018-06-19 18:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` enum('email') NOT NULL,
  `data` text NOT NULL,
  `status` enum('pending','started','failed','done') NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `created_at`, `type`, `data`, `status`) VALUES
(1, '2018-06-13 13:19:22', 'email', '{\"to\":[\"uros@gmail.com\"],\"cc\":[],\"bcc\":[],\"att\":[],\"subject\":\"Uspesna registracija\",\"body\":\"<!doctype html><html><head><meta charset=\\\"utf-8\\\"><body><p>Uspesno ste se registrovali: Uros Arnaut<\\/p><p>Vase korisnicko ime je : uros1234<\\/p><\\/body><\\/html>\",\"attempt\":0}', 'done'),
(2, '2018-06-13 20:12:58', 'email', '{\"to\":[\"stancha7@gmail.com\"],\"cc\":[],\"bcc\":[],\"att\":[],\"subject\":\"Uspesna registracija\",\"body\":\"<!doctype html><html><head><meta charset=\\\"utf-8\\\"><body><p>Uspesno ste se registrovali: Marko Stancic<\\/p><p>Vase korisnicko ime je : marko1234<\\/p><\\/body><\\/html>\",\"attempt\":0}', 'done'),
(3, '2018-06-17 19:42:03', 'email', '{\"to\":[\"stancic6@gmail.com\"],\"cc\":[],\"bcc\":[],\"att\":[],\"subject\":\"Uspesna registracija\",\"body\":\"<!doctype html><html><head><meta charset=\\\"utf-8\\\"><body><p>Uspesno ste se registrovali: Marko Stancic<\\/p><p>Vase korisnicko ime je : mstancic<\\/p><\\/body><\\/html>\",\"count\":0}', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(24) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `user_id`, `ip_address`, `created_at`) VALUES
(1, 3, '::1', '2018-05-14 09:34:45'),
(2, 3, '::1', '2018-05-14 09:35:12'),
(3, 3, '::1', '2018-05-14 09:35:30'),
(4, 3, '::1', '2018-05-14 10:40:13'),
(5, 3, '::1', '2018-05-14 11:16:48'),
(6, 3, '::1', '2018-05-17 14:00:05'),
(7, 3, NULL, '2018-05-29 08:01:37'),
(8, 3, NULL, '2018-05-29 09:35:57'),
(9, 3, '::1', '2018-05-29 11:53:35'),
(10, 3, '::1', '2018-05-30 11:39:12'),
(11, 3, '::1', '2018-05-30 12:40:35'),
(12, 3, '::1', '2018-05-30 13:19:45'),
(13, 3, '::1', '2018-05-31 10:14:28'),
(14, 3, '::1', '2018-06-04 08:51:33'),
(15, 3, '::1', '2018-06-05 12:43:40'),
(16, 3, '::1', '2018-06-06 12:39:40'),
(17, 3, '::1', '2018-06-06 12:45:23'),
(18, 3, '::1', '2018-06-06 13:34:26'),
(19, 2, '::1', '2018-06-06 13:43:34'),
(20, 3, '::1', '2018-06-07 10:05:43'),
(21, 3, '::1', '2018-06-07 10:23:06'),
(22, 3, '::1', '2018-06-07 11:13:57'),
(23, 3, '::1', '2018-06-07 11:14:22'),
(24, 3, '::1', '2018-06-07 11:19:09'),
(25, 3, '::1', '2018-06-07 11:43:30'),
(26, 3, '::1', '2018-06-11 07:38:35'),
(27, 3, '::1', '2018-06-11 08:12:21'),
(28, 3, '::1', '2018-06-11 08:15:22'),
(29, 3, '::1', '2018-06-11 09:14:15'),
(30, 3, '::1', '2018-06-11 09:48:46'),
(31, 3, '::1', '2018-06-11 12:28:50'),
(32, 3, '::1', '2018-06-12 09:42:35'),
(33, 3, '::1', '2018-06-12 11:17:52'),
(34, 3, '::1', '2018-06-12 11:18:29'),
(35, 3, '::1', '2018-06-12 11:19:02'),
(36, 3, '::1', '2018-06-12 11:43:43'),
(37, 3, '::1', '2018-06-12 11:45:45'),
(38, 3, '::1', '2018-06-13 10:49:12'),
(39, 8, '::1', '2018-06-13 13:33:14'),
(40, 3, '::1', '2018-06-13 18:18:18'),
(41, 3, '::1', '2018-06-13 18:51:12'),
(42, 3, '::1', '2018-06-13 21:34:01'),
(43, 3, '::1', '2018-06-13 21:36:23'),
(44, 3, '::1', '2018-06-14 20:26:06'),
(45, 3, '::1', '2018-06-14 21:22:43'),
(46, 3, '::1', '2018-06-14 22:11:21'),
(47, 3, '::1', '2018-06-15 16:54:19'),
(48, 3, '::1', '2018-06-15 20:04:36'),
(49, 3, '::1', '2018-06-15 20:19:27'),
(50, 3, '::1', '2018-06-15 21:21:38'),
(51, 3, '::1', '2018-06-15 21:21:48'),
(52, 3, '::1', '2018-06-15 21:29:22'),
(53, 3, '::1', '2018-06-15 21:38:12'),
(54, 2, '::1', '2018-06-15 22:17:33'),
(55, 3, '::1', '2018-06-15 22:37:52'),
(56, 3, '::1', '2018-06-15 23:00:51'),
(57, 3, '::1', '2018-06-15 23:01:01'),
(58, 3, '::1', '2018-06-16 11:17:21'),
(59, 3, '::1', '2018-06-16 11:19:43'),
(60, 3, '::1', '2018-06-16 11:20:34'),
(61, 3, '::1', '2018-06-19 16:16:30'),
(62, 3, '::1', '2018-06-19 16:21:05'),
(63, 3, '::1', '2018-06-19 17:05:13'),
(64, 3, '::1', '2018-06-19 18:40:21'),
(65, 3, '::1', '2018-06-21 01:48:31'),
(66, 3, '::1', '2018-06-21 01:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `manufacturer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `created_at`) VALUES
(1, 'Ford', '2018-06-15 17:47:40'),
(2, 'Citroen', '2018-06-15 17:47:49'),
(6, 'Audi', '2018-06-15 17:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `part`
--

CREATE TABLE `part` (
  `part_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `price` decimal(10,2) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `is_warranty_given` tinyint(1) DEFAULT NULL,
  `warranty_month` int(11) DEFAULT NULL,
  `car_id` int(10) UNSIGNED DEFAULT NULL,
  `is_in_stock` tinyint(1) DEFAULT NULL,
  `image_path` varchar(24) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `part`
--

INSERT INTO `part` (`part_id`, `name`, `description`, `price`, `category_id`, `user_id`, `is_warranty_given`, `warranty_month`, `car_id`, `is_in_stock`, `image_path`, `created_at`) VALUES
(5, 'far citroen ds4', 'detaljnije...', '200.00', 2, 3, 0, 0, 2, 1, '5.jpg', '2018-06-21 02:51:25'),
(6, 'felna citroen c5', 'detaljnije...', '150.00', 2, 3, 0, 0, 2, NULL, '6.jpg', '2018-06-21 02:51:30'),
(7, 'volan citroen c3', 'detaljnije...', '100.00', 2, 3, 1, 1, 2, NULL, '7.jpg', '2018-06-16 18:27:23'),
(31, 'far', 'detaljnije...', '500.00', 1, 3, 1, 1, 3, NULL, '31.jpg', '2018-06-16 18:27:28'),
(36, 'felna audi s3', 'detaljnije...', '200.00', 1, 3, 1, 1, 3, NULL, '36.jpg', '2018-06-16 18:27:36'),
(37, 'volan audi q7', 'detaljnije...', '200.00', 1, 3, 0, 0, 3, NULL, '37.jpg', '2018-06-21 02:51:33'),
(53, 'abv', 'abv', '123.00', 1, 3, 0, 0, 1, 0, '53.jpg', '2018-06-13 19:01:44');

-- --------------------------------------------------------

--
-- Stand-in structure for view `parts_show`
-- (See below for the actual view)
--
CREATE TABLE `parts_show` (
`part_id` int(10) unsigned
,`name` varchar(255)
,`description` text
,`price` decimal(10,2) unsigned
,`is_warranty_given` tinyint(1)
,`warranty_month` int(11)
,`is_in_stock` tinyint(1)
,`image_path` varchar(24)
,`category` varchar(255)
,`model` varchar(255)
,`manufacturer` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `part_show`
-- (See below for the actual view)
--
CREATE TABLE `part_show` (
`name` varchar(255)
,`description` text
,`price` decimal(10,2) unsigned
,`is_warranty_given` tinyint(1)
,`warranty_month` int(11)
,`is_in_stock` tinyint(1)
,`image_path` varchar(24)
,`category` varchar(255)
,`model` varchar(255)
,`manufacturer` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `forename` varchar(255) DEFAULT NULL,
  `surename` varchar(255) DEFAULT NULL,
  `address` text,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `forename`, `surename`, `address`, `phone`, `created_at`) VALUES
(1, 'dsasd', 'asdas', 'asdas', 'asdas', 'asdas', 'asdas', 'asda', NULL),
(2, 'markomarko', 'test@test.rs', '$2y$10$Hydh1bvsyuNjhMKqK8mmhegsStgknig3sQWhwsmarshDxOsRUM5i.', 'asdas', 'asdas', NULL, NULL, NULL),
(3, 'marko_3', 'test@test.com', '$2y$10$6brvdGt.IJGDFpYT/vIaPuFxVgYEwMvmGvzFqEcIVZ4FlQX9/0Xfi', 'asdas', 'asdas', NULL, NULL, NULL),
(7, 'marko1995', 'stancic6@gmail.coms', '$2y$10$e83tHL8mvkQImeYHY57zXel5ZEe/a5wjaIE1jV0GdmI7e8yxU1NoG', 'marko', 'stancic', NULL, NULL, '2018-06-17 18:39:26'),
(8, 'uros1234', 'uros@gmail.com', '$2y$10$C7IDEtCmjFHVLCJrlTtVo..VJCH.dClR7do11yyUcdpOVrgdr25J2', 'Uros', 'Arnaut', NULL, NULL, '2018-06-13 13:19:21'),
(9, 'marko1234', 'stancha7@gmail.com', '$2y$10$6/w1HpolGAKyphjwh7NFneqmXM3JIEBwqja3TR.Pm/3mQixAr.EHa', 'Marko', 'Stancic', NULL, NULL, '2018-06-13 20:12:58'),
(10, 'marina_i_marko', 'test@test.sadas', '$2y$10$JXmqRJ7XygJu8lFj4sn43OvzuR8L.PlX5iKghfwrN/QoVdP1HdrXi', 'Marina', 'Stancic', NULL, NULL, '2018-06-17 17:52:39'),
(11, 'mstancic', 'stancic6@gmail.com', '$2y$10$8klncijKQQiUPukPww2of.qBuAPACGxjG7irTaPP9BgdfKhTM2vQu', 'Marko', 'Stancic', NULL, NULL, '2018-06-17 19:42:03');

-- --------------------------------------------------------

--
-- Structure for view `cart_part_view`
--
DROP TABLE IF EXISTS `cart_part_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cart_part_view`  AS  select `cart_part`.`cart_part_id` AS `cart_part_id`,`part`.`name` AS `name`,`cart`.`created_at` AS `created_at`,`cart`.`forename` AS `forename`,`cart`.`surename` AS `surename`,`cart`.`address` AS `address`,`cart`.`email` AS `email`,`cart`.`cart_id` AS `cart_id` from ((`part` join `cart_part` on((`part`.`part_id` = `cart_part`.`part_id`))) join `cart` on((`cart_part`.`cart_id` = `cart`.`cart_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `parts_show`
--
DROP TABLE IF EXISTS `parts_show`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `parts_show`  AS  select `part`.`part_id` AS `part_id`,`part`.`name` AS `name`,`part`.`description` AS `description`,`part`.`price` AS `price`,`part`.`is_warranty_given` AS `is_warranty_given`,`part`.`warranty_month` AS `warranty_month`,`part`.`is_in_stock` AS `is_in_stock`,`part`.`image_path` AS `image_path`,`category`.`name` AS `category`,`car`.`model` AS `model`,`manufacturer`.`name` AS `manufacturer` from (((`manufacturer` join `car` on((`manufacturer`.`manufacturer_id` = `car`.`manufacturer_id`))) join `part` on((`car`.`car_id` = `part`.`car_id`))) join `category` on((`category`.`category_id` = `part`.`category_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `part_show`
--
DROP TABLE IF EXISTS `part_show`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `part_show`  AS  select `part`.`name` AS `name`,`part`.`description` AS `description`,`part`.`price` AS `price`,`part`.`is_warranty_given` AS `is_warranty_given`,`part`.`warranty_month` AS `warranty_month`,`part`.`is_in_stock` AS `is_in_stock`,`part`.`image_path` AS `image_path`,`category`.`name` AS `category`,`car`.`model` AS `model`,`manufacturer`.`name` AS `manufacturer` from (((`manufacturer` join `car` on((`manufacturer`.`manufacturer_id` = `car`.`manufacturer_id`))) join `part` on((`car`.`car_id` = `part`.`car_id`))) join `category` on((`category`.`category_id` = `part`.`category_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`car_id`),
  ADD KEY `fk_car_manufacturer_id` (`manufacturer_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `cart_part`
--
ALTER TABLE `cart_part`
  ADD PRIMARY KEY (`cart_part_id`),
  ADD KEY `fk_cart_part_cart_id` (`cart_id`),
  ADD KEY `fk_cart_part_part_id` (`part_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `uq_category_name` (`name`) USING BTREE;

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `fk_log_user_id` (`user_id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`),
  ADD UNIQUE KEY `uq_manufacturer_name` (`name`) USING BTREE;

--
-- Indexes for table `part`
--
ALTER TABLE `part`
  ADD PRIMARY KEY (`part_id`),
  ADD UNIQUE KEY `uq_part_name` (`name`) USING BTREE,
  ADD KEY `fk_part_category_id` (`category_id`),
  ADD KEY `fk_part_user_id` (`user_id`),
  ADD KEY `fk_user_car_id` (`car_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `uq_user_username` (`username`) USING BTREE,
  ADD UNIQUE KEY `uq_user_email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `car_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `cart_part`
--
ALTER TABLE `cart_part`
  MODIFY `cart_part_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `manufacturer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `part`
--
ALTER TABLE `part`
  MODIFY `part_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `car`
--
ALTER TABLE `car`
  ADD CONSTRAINT `fk_car_manufacturer_id` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`manufacturer_id`) ON UPDATE CASCADE;

--
-- Constraints for table `cart_part`
--
ALTER TABLE `cart_part`
  ADD CONSTRAINT `fk_cart_part_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cart_part_part_id` FOREIGN KEY (`part_id`) REFERENCES `part` (`part_id`) ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `fk_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `part`
--
ALTER TABLE `part`
  ADD CONSTRAINT `fk_part_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_part_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_car_id` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
