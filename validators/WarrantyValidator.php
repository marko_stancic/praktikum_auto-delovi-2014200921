<?php
    namespace App\Validators;

    class WarrantyValidator implements \App\Core\Validator {

        private $integerLength;

        public function __construct(){
            $this->integerLength = 10;

        }
        public function &setIntegerLength(int $length): WarrantyValidator {
            $this->integerLength = max(1, $length);
            return $this;
        }
        public function isValid(string $value): bool {
            $pattern = '/^[0-9]{0,'. ($this->integerLength-1) .'}$/';

            return \boolval(\preg_match($pattern, $value));
        }
    }