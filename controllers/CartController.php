<?php
    namespace App\Controllers;

    class CartController extends \App\Core\Role\UserRoleController {
        public function home(){
            $cartModel = new \App\Models\CartModel($this->getDatabaseConnection());
            $carts = $cartModel->getAll();

            $this->set('carts', $carts);

        }
        
        public function delete(int $cartId){
            $cartModel = new \App\Models\CartModel($this->getDatabaseConnection());            
            $res = $cartModel->deleteById($cartId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisana artikal iz korpe');
                return;
            }

            $this->redirect(\Configuration::BASE .'user/cart');
        }
    }