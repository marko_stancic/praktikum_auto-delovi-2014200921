<?php
    namespace App\Controllers;

    class ManufacturerController extends \App\Core\Controller{

        public function show($id){
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturer = $manufacturerModel->getById($id);

            if(!$manufacturer){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('manufacturer', $manufacturer);
        }
    }