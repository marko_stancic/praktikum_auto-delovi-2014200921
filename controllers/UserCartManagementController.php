<?php
    namespace App\Controllers;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Models\CartModel;

    class UserCartManagementController extends \App\Core\Controller {
        public function getCart(){

        }
        public function postCart(){
            $email = \filter_input(INPUT_POST, 'input_email', FILTER_SANITIZE_EMAIL);
            $forename = \filter_input(INPUT_POST, 'input_forename', FILTER_SANITIZE_STRING);
            $surename = \filter_input(INPUT_POST, 'input_surename', FILTER_SANITIZE_STRING);
            $address = \filter_input(INPUT_POST, 'input_address', FILTER_SANITIZE_STRING);
            $phone = \filter_input(INPUT_POST, 'input_phone', FILTER_SANITIZE_STRING);
            $cartModel = new CartModel($this->getDatabaseConnection());

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($forename)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($surename)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(32);
            if(!$stringValidator->isValid($address)){
               $this->set('message', 'Doslo je do greske: Adresa nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(32);
            if(!$stringValidator->isValid($address)){
               $this->set('message', 'Doslo je do greske: Adresa nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($email)){
               $this->set('message', 'Doslo je do greske: Email nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(32);
            if(!$stringValidator->isValid($phone)){
               $this->set('message', 'Doslo je do greske: Telefon nije ispravnog formata!');
                return; 
            }

            $parts = $this->getSession()->get('carts', []);

            $cartId = $cartModel->add([
                'forename' => $forename,
                'surename' => $surename,
                'email' => $email,
                'address' => $address,
                'phone' => $phone
            ]);

            if (!$cartId){
                $this->set('message', 'Doslo je do greske: Nije bilo uspesno dodavanje u korpu!');
                return;
            }

            $cartPartModel = new \App\Models\CartPartModel($this->getDatabaseConnection());

            foreach ($parts as $part) {
                $cartPartId = $cartPartModel->add([
                    'cart_id' => $cartId,
                    'part_id' => $part->part_id
                ]);
            }

            $parts = $this->getSession()->put('carts', []);
            $msg = "Uspesno ste dodali u korpu! Vaš broj porudzbine je: " . $cartId;
            $this->set('message', $msg);
        }
        
    }