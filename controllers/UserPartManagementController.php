<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\WarrantyValidator;
    use App\Validators\BitValidator;
    use App\Models\PartModel;

    class UserPartManagementController extends UserRoleController {
        public function parts() {
            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
            $parts = $partModel->getAll();

            $this->set('parts', $parts);
        }

        public function getEdit($partId){
            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
            $part = $partModel->getById($partId);

            if( !$part ){
                $this->redirect(\Configuration::BASE . 'user/parts');
            }

            $this->set('part', $part);

            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);

            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();

            $this->set('cars', $cars);

            return $partModel;
        }

        public function postEdit($partId){
            $partModel =  $this->getEdit($partId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $isInStock = intval(filter_input(INPUT_POST, 'is_in_stock', FILTER_SANITIZE_NUMBER_INT));
            $isWarrantyGiven = intval(filter_input(INPUT_POST, 'is_warranty_given', FILTER_SANITIZE_NUMBER_INT));
            $warrantyMonth = filter_input(INPUT_POST, 'warranty_month', FILTER_SANITIZE_NUMBER_INT);
            $carId = filter_input(INPUT_POST, 'car_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime auto dela nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMaxLength(6*1024);
            if(!$stringValidator->isValid($description)){
               $this->set('message', 'Doslo je do greske: Opis nije ispravnog formata!');
                return; 
            }
            $numberValidator = (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2);
            if(!$stringValidator->isValid($price)){
               $this->set('message', 'Doslo je do greske: Cena nije ispravnog formata!');
                return; 
            }
            $warrantyValidator = (new WarrantyValidator())->setIntegerLength(11);
            if(!$warrantyValidator->isValid($warrantyMonth)){
               $this->set('message', 'Doslo je do greske: Garancija nije ispravnog formata!');
                return; 
            }
            $bitValidator = (new BitValidator());
            if(!$bitValidator->isValid($isInStock)){
               $this->set('message', 'Doslo je do greske: Stanje nije ispravnog formata!');
                return; 
            }
            $bitValidator = (new BitValidator());
            if(!$bitValidator->isValid($isWarrantyGiven)){
               $this->set('message', 'Doslo je do greske: Stanje nije ispravnog formata!');
                return; 
            }


            $partModel = new PartModel($this->getDatabaseConnection());
            $part = $partModel->getByFieldName('name', $name);
            if($part) {
                $this->set('message', 'Doslo je do greske: Vec postoji deo sa tim imenom!');
                return; 
            }

            $res = $partModel->editById($partId, [
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'is_in_stock' => $isInStock,
                'user_id' => $this->getSession()->get('user_id'),
                'is_warranty_given' => $isWarrantyGiven,
                'warranty_month' => $warrantyMonth,
                'car_id' => $carId
            ]);

            if( !$res ){
                $this->set('message', 'Doslo je do greske: Nije moguce izmeniti deo!');
            }

            if($_FILES['image_path'] && $_FILES['image_path']['error'] == 0){
                $uploadStatus = $this->doImageUpload('image_path', $partId);
                if(!$uploadStatus) {
                    return;
                }
            }

            $this->redirect(\Configuration::BASE . 'user/parts');
        }

        public function getAdd(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);

            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();

            $this->set('cars', $cars);
        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            $price = sprintf("%.2f", \filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $isInStock = intval(filter_input(INPUT_POST, 'is_in_stock', FILTER_SANITIZE_NUMBER_INT));
            $isWarrantyGiven = intval(filter_input(INPUT_POST, 'is_warranty_given', FILTER_SANITIZE_NUMBER_INT));
            $warrantyMonth = filter_input(INPUT_POST, 'warranty_month', FILTER_SANITIZE_NUMBER_INT);
            $carId = filter_input(INPUT_POST, 'car_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime auto dela nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMaxLength(6*1024);
            if(!$stringValidator->isValid($description)){
               $this->set('message', 'Doslo je do greske: Opis nije ispravnog formata!');
                return; 
            }
            $numberValidator = (new NumberValidator())->setDecimal()->setUnsigned()->setIntegerLength(7)->setMaxDecimalDigits(2);
            if(!$stringValidator->isValid($price)){
               $this->set('message', 'Doslo je do greske: Cena nije ispravnog formata!');
                return; 
            }
            $warrantyValidator = (new WarrantyValidator())->setIntegerLength(11);
            if(!$warrantyValidator->isValid($warranty_month)){
               $this->set('message', 'Doslo je do greske: Garancija nije ispravnog formata!');
                return; 
            }
            $bitValidator = (new BitValidator());
            if(!$bitValidator->isValid($isInStock)){
               $this->set('message', 'Doslo je do greske: Stanje nije ispravnog formata!');
                return; 
            }
            $bitValidator = (new BitValidator());
            if(!$bitValidator->isValid($isWarrantyGiven)){
               $this->set('message', 'Doslo je do greske: Stanje nije ispravnog formata!');
                return; 
            }

            $partModel = new PartModel($this->getDatabaseConnection());
            $part = $partModel->getByFieldName('name', $name);
            if($part) {
                $this->set('message', 'Doslo je do greske: Vec postoji deo sa tim imenom!');
                return; 
            }

            $partId = $partModel->add([
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'category_id' => $categoryId,
                'is_in_stock' => $isInStock,
                'user_id' => $this->getSession()->get('user_id'),
                'is_warranty_given' => $isWarrantyGiven,
                'warranty_month' => $warrantyMonth,
                'car_id' => $carId
            ]);
        

            if( !$partId ){
                $this->set('message', 'Doslo je do greske: Nije moguce dodati deo!');
            }
            $uploadStatus = $this->doImageUpload('image_path', $partId);
            if(!$uploadStatus) {
                return;
            }
            $this->redirect(\Configuration::BASE . 'user/parts');
            
        }
        private function doImageUpload(string $fieldName, string $partId){
            $partModel = new PartModel($this->getDatabaseConnection());
            $part = $partModel->getById(intval($partId));

            unlink(\Configuration::UPLOAD_DIR . $part->image_path);

            $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
            $file = new \Upload\File($fieldName, $uploadPath);
            $file->setName($partId);
            $file->addValidations([
                new \Upload\Validation\Mimetype(["image/jpeg", "image/png"]),
                new \Upload\Validation\Size("5M")
            ]);

            try{
                $file->upload();

                $fullName = $file->getNameWithExtension();

                $stringValidator = (new StringValidator())->setMaxLength(120);
                if(!$stringValidator->isValid($fullName)){
                $this->set('message', 'Doslo je do greske: Putanja nije ispravnog formata!');
                    return; 
                }

                $partModel->editById(intval($partId), [
                    'image_path' => $fullName
                ]);
                
                $this->doResize(
                    \Configuration::UPLOAD_DIR . $fullName,
                    \Configuration::DEFAULT_IMAGE_WIDTH,
                    \Configuration::DEFAULT_IMAGE_HEIGHT
                );

                return true;
            } catch (Exception $e) {
                $this->set('message', 'Greska' . \implode(', ', $file.getErrors()) );
                return false;
            }
        }
        private function doResize(string $filePath, int $w, int $h){
            $longer = max($w, $h);

            $image = new \Gumlet\ImageResize($filePath);
            $image->resizeToBestFit($longer, $longer);
            $image->crop($w, $h);
            $image->save($filePath);
        }
        public function delete(int $partId){
            $partModel = new PartModel($this->getDatabaseConnection());            
            $res = $partModel->deleteById($partId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan proizvodjac');
                return;
            }

            $this->redirect(\Configuration::BASE .'user/parts');
        }
    }