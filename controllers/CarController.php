<?php
    namespace App\Controllers;

    class CarController extends \App\Core\Controller{

        public function show($id){
            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $car = $carModel->getById($id);

            if(!$car){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('car', $car);
        }
    }