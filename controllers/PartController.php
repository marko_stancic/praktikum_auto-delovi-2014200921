<?php
    namespace App\Controllers;

    class PartController extends \App\Core\Controller{

        public function show($id){
            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
            $parts = $partModel->getAllByPartId($id);

            if(!$parts){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('parts', $parts);
        }
        private function normaliseKeywords(string $keywords): string {
            $keywords = \trim($keywords);
            $keywords = \preg_replace('/ +/', ' ', $keywords);

            return $keywords;
        }

        public function postSearch(){
            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);
            $keywords = $this->normaliseKeywords($q);

            $parts = $partModel->getAllBySearch($keywords);

            $this->set('parts', $parts);
        }
        public function postFilter(){
            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();
            $this->set('cars', $cars);
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);

            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
    
            $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);
            $manufacturerId = filter_input(INPUT_POST, 'manufacturer_id', FILTER_SANITIZE_NUMBER_INT);
            $carId = filter_input(INPUT_POST, 'car_id', FILTER_SANITIZE_NUMBER_INT);
            $price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
            $price_to = filter_input(INPUT_POST, 'price_to', FILTER_SANITIZE_STRING);

            $parts = $partModel->getAllByFilter($carId, $categoryId, $manufacturerId,  $price, $price_to);
    
            $this->set('parts', $parts);
        }
    }

    