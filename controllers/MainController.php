<?php
    namespace App\Controllers;

    use App\Validators\StringValidator;
    use App\Models\UserModel;

    class MainController extends \App\Core\Controller{

        public function home(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);

            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();
            $this->set('cars', $cars);

            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);
        }
        public function about_us(){
            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();
            $this->set('cars', $cars);
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);
        }
        public function contact(){
            $carModel = new \App\Models\CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();
            $this->set('cars', $cars);
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);
        }

        public function getRegister(){

        }
        public function postRegister(){
            $email = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
            $forename = \filter_input(INPUT_POST, 'reg_forename', FILTER_SANITIZE_STRING);
            $surename = \filter_input(INPUT_POST, 'reg_surename', FILTER_SANITIZE_STRING);
            $username = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
            $password1 = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2 = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

            if($password1 !== $password2){
                $this->set('message', 'Doslo je do greske: Niste uneli dva puta istu lozinku!');
                return;
            }

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$stringValidator->isValid($password1)){
               $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return; 
            }
    
            $userModel = new UserModel($this->getDatabaseConnection());
            $user = $userModel->getByFieldName('email', $email);
            if($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tom email adresom!');
                return; 
            }

            $user = $userModel->getByFieldName('username', $username);
            if($user) {
                $this->set('message', 'Doslo je do greske: Vec postoji korisnik sa tim korisnickim imenom!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($forename)){
               $this->set('message', 'Doslo je do greske: Ime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($surename)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($email)){
               $this->set('message', 'Doslo je do greske: Prezime nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(120);
            if(!$stringValidator->isValid($username)){
               $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$stringValidator->isValid($password2)){
               $this->set('message', 'Doslo je do greske: Ponovljena lozinka nije ispravnog formata!');
                return; 
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $userId =$userModel->add([
                'username' => $username,
                'password' => $passwordHash,
                'email' => $email,
                'forename' => $forename,
                'surename' => $surename
            ]);


            if(!$userId){
                $this->set('message', 'Doslo je do greske: Nije bilo uspesno registrovanje korisnika!');
                return;
            }

            $this->set('message', 'Uspesno ste se registrovali!');
            
            $this->notifyUser($userId);

            header(\Configuration::BASE);
        }

        public function getLogin(){

        }
        public function postLogin(){
            $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(7)->setMaxLength(124);
            if(!$stringValidator->isValid($password)){
               $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata!');
                return; 
            }
            $userModel = new UserModel($this->getDatabaseConnection());
            $user = $userModel->getByFieldName('username', $username);
            if(!$user) {
                $this->set('message', 'Doslo je do greske: Ne postoji korisnik sa tim korisnickim imenom!');
                return; 
            }
            $stringValidator = (new StringValidator())->setMinLength(5)->setMaxLength(120);
            if(!$stringValidator->isValid($username)){
               $this->set('message', 'Doslo je do greske: Korisnicko ime nije ispravnog formata!');
                return; 
            }
            

            if(!password_verify($password, $user->password)){
                sleep(1);
                $this->set('message', 'Doslo je do greske: Lozinka nije ispravna!');
                return;
            }



            $this->getSession()->put('user_id', $user->user_id);
            $this->getSession()->save();
           
            $logModel = new \App\Models\LogModel($this->getDatabaseConnection());
            
            $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            
            $log = $logModel->add(
                [
                    'user_id' => $this->getSession()->get('user_id'),
                    'ip_address' => $ipAddress
                ]
            );

            $this->redirect(\Configuration::BASE . 'user/profile');


        }
        public function logout(){
            $this->getSession()->remove('user_id');
            $this->getSession()->save();
            $this->getSession()->regenerate();

            $this->redirect(\Configuration::BASE);
        }
        private function notifyUser(int $userId){
            $userModel = new UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($userId);
            
            $user = $userModel->getById($user->user_id);
            $html = '<!doctype html><html><head><meta charset="utf-8"><body>';
            $html .= '<p>Uspesno ste se registrovali: ' . \htmlspecialchars($user->forename) .' '. \htmlspecialchars($user->surename) . '</p>';
            $html .= '<p>Vase korisnicko ime je : ' . \htmlspecialchars($user->username) . '</p>';
            $html .= '</body></html>';

            $event = new \App\EventHandlers\EmailEventHandler();
            $event->setSubject('Uspesna registracija');
            $event->setBody($html);
            $event->addAddress($user->email);

            $eventModel = new \App\Models\EventModel($this->getDatabaseConnection());
            $eventModel->add([
                'type' => 'email',
                'data' => $event->getData()
            ]);

        }

    }