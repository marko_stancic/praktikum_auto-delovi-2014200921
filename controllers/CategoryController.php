<?php
    namespace App\Controllers;

    class CategoryController extends \App\Core\Controller{

        public function show($id){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            if(!$category){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('category', $category);

            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
            $partsInCategory = $partModel->getAllByCategoryId($id);

            $this->set('partsInCategory', $partsInCategory);

        }
    }