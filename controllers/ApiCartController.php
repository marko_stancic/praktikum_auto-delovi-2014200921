<?php
    namespace App\Controllers;

    class ApiCartController extends \App\Core\ApiController {
        public function showCart() {
            $carts = $this->getSession()->get('carts', []);
            $this->set('carts', $carts);
        }

        public function addToCart($partId) {
            $partModel = new \App\Models\PartModel($this->getDatabaseConnection());
            $part = $partModel->getById($partId);

            if (!$part) {
                $this->set('error', -1);
                return;
            }

            $carts = $this->getSession()->get('carts', []);

            foreach ($carts as $cart) {
                if ($cart->part_id == $partId) {
                    $this->set('error', -2);
                    return;
                }
            }

            $carts[] = $part;
            $this->getSession()->put('carts', $carts);

            $this->set('error', 0);
        }

        public function clear() {
            $this->getSession()->put('carts', []);

            $this->set('error', 0);
        }
    }
