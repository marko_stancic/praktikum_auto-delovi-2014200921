<?php
    namespace App\Controllers;

    class CartPartController extends \App\Core\Role\UserRoleController {
        public function show($id){
            $cartPartModel = new \App\Models\CartPartModel($this->getDatabaseConnection());
            $cartPart = $cartPartModel->getByCartId($id);

            if(!$cartPart){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('cartPart', $cartPart);

        }
    }