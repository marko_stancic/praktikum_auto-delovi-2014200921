<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Models\CarModel;
    

    class UserCarManagementController extends UserRoleController {

        public function cars() {
            $carModel = new CarModel($this->getDatabaseConnection());
            $cars = $carModel->getAll();

            $this->set('cars', $cars);
        }

        public function getEdit($carId){
            $carModel = new CarModel($this->getDatabaseConnection());
            $car = $carModel->getById($carId);

            if( !$car ){
                $this->redirect(\Configuration::BASE . 'user/cars');
            }

            $this->set('car', $car);
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);

            return $carModel;
        }

        public function postEdit($carId){
            $carModel = new CarModel($this->getDatabaseConnection());
            $carModel =  $this->getEdit($carId);

            $model = \filter_input(INPUT_POST, 'model', FILTER_SANITIZE_STRING);
            $manufacturerId = \filter_input(INPUT_POST, 'manufacturer_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($model)){
               $this->set('message', 'Doslo je do greske: Model nije ispravnog formata!');
                return; 
            }

            $carModel->editById($carId, [
                'model' => $model,
                'manufacturer_id' => $manufacturerId
            ]);

            $this->redirect(\Configuration::BASE . 'user/cars');
        }

        public function getAdd(){
            $manufacturerModel = new \App\Models\ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();
            $this->set('manufacturers', $manufacturers);
        }

        public function postAdd(){
            $model = \filter_input(INPUT_POST, 'model', FILTER_SANITIZE_STRING);
            $manufacturerId = \filter_input(INPUT_POST, 'manufacturer_id', FILTER_SANITIZE_NUMBER_INT);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($model)){
               $this->set('message', 'Doslo je do greske: Model nije ispravnog formata!');
                return; 
            }


            $carModel = new CarModel($this->getDatabaseConnection());

            $carId = $carModel->add([
                'model' => $model,
                'manufacturer_id' => $manufacturerId
            ]);

            if( $carId ){
                 $this->redirect(\Configuration::BASE . 'user/cars');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati auto!');

            
        }
        public function delete(int $carId){
            $carModel = new CarModel($this->getDatabaseConnection());            
            $res = $carModel->deleteById($carId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan automobil');
                return;
            }

            $this->redirect(\Configuration::BASE .'user/cars');
        }
    }