<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;
    use App\Validators\StringValidator;
    use App\Models\ManufacturerModel;

    class UserManufacturerManagementController extends UserRoleController {

        public function manufacturers() {
            $manufacturerModel = new ManufacturerModel($this->getDatabaseConnection());
            $manufacturers = $manufacturerModel->getAll();

            $this->set('manufacturers', $manufacturers);
        }

        public function getEdit($manufacturerId){
            $manufacturerModel = new ManufacturerModel($this->getDatabaseConnection());
            $manufacturer = $manufacturerModel->getById($manufacturerId);

            if( !$manufacturer ){
                $this->redirect(\Configuration::BASE . 'user/manufacturers');
            }

            $this->set('manufacturer', $manufacturer);

            return $manufacturerModel;
        }

        public function postEdit($manufacturerId){
            $manufacturerModel =  $this->getEdit($manufacturerId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime proizvodjaca nije ispravnog formata!');
                return; 
            }

            $manufacturerModel->editById($manufacturerId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'user/manufacturers');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime proizvodjaca nije ispravnog formata!');
                return; 
            }

            $manufacturerModel = new ManufacturerModel($this->getDatabaseConnection());          
            $manufacturer = $manufacturerModel->getByFieldName('name', $name);
            if($manufacturer) {
                $this->set('message', 'Doslo je do greske: Vec postoji proizvodjac sa tim imenom!');
                return; 
            }
            $manufacturerId = $manufacturerModel->add([
                'name' => $name
            ]);

            if( $manufacturerId ){
                 $this->redirect(\Configuration::BASE . 'user/manufacturers');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati proizvodjaca!');
        }
        public function delete(int $manufacturerId){
            $manufacturerModel = new ManufacturerModel($this->getDatabaseConnection());            
            $res = $manufacturerModel->deleteById($manufacturerId);

            if( !$res){
                $this->set('message', 'Došlo je do greške: Nije obrisan proizvodjac');
                return;
            }

            $this->redirect(\Configuration::BASE .'user/manufacturers');
        }
    }