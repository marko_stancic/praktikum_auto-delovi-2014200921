<?php
    namespace App\Controllers;
    use App\Core\Role\UserRoleController;
    use App\Validators\StringValidator;
    use App\Models\CategoryModel;

    class UserCategoryManagementController extends UserRoleController {

        public function categories() {
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();

            $this->set('categories', $categories);
        }

        public function getEdit($categoryId){
            $categoryModel = new CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($categoryId);

            if( !$category ){
                $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('category', $category);

            return $categoryModel;
        }

        public function postEdit($categoryId){
            $categoryModel =  $this->getEdit($categoryId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);


            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime kategorije nije ispravnog formata!');
                return; 
            }

            $categoryModel->editById($categoryId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'user/categories');
        }

        public function getAdd(){

        }

        public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $categoryModel = new CategoryModel($this->getDatabaseConnection());            
            $category = $categoryModel->getByFieldName('name', $name);
            if($category) {
                $this->set('message', 'Doslo je do greske: Vec postoji kategorija sa tim imenom!');
                return; 
            }

            $stringValidator = (new StringValidator())->setMinLength(3)->setMaxLength(120);
            if(!$stringValidator->isValid($name)){
               $this->set('message', 'Doslo je do greske: Ime kategorije nije ispravnog formata!');
                return; 
            }

            $categoryId = $categoryModel->add([
                'name' => $name
            ]);

            if( $categoryId ){
                 $this->redirect(\Configuration::BASE . 'user/categories');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati kategoriju!');
        }
        public function delete(int $categoryId){
            $categoryModel = new CategoryModel($this->getDatabaseConnection());            
            $category = $categoryModel->deleteById($categoryId);

            if( !$category){
                $this->set('message', 'Došlo je do greške: Nije obrisana kategorija');
                return;
            }

            $this->redirect(\Configuration::BASE .'user/categories');
        }
    }